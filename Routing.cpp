
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/adapted/c_array.hpp>
BOOST_GEOMETRY_REGISTER_C_ARRAY_CS(cs::cartesian)
using namespace boost::geometry;


#include "Routing.h"

//#include <QStringList>
#include <QDebug>
#include <cmath>
#include <algorithm>
#include "polyline.h"

#include <boost/numeric/odeint.hpp>
using namespace std;
using namespace boost::numeric::odeint;


/* we solve the simple ODE x' = 3/(2t^2) + x/(2t)
 * with initial condition x(1) = 0.
 * Analytic solution is x(t) = sqrt(t) - 1/t
 */

void rhs( const double x , double &dxdt , const double t )
{
    dxdt = 3.0/(2.0*t*t) + x/(2.0*t);
}

void write_cout( const double &x , const double t )
{
    cout << t << '\t' << x << endl;
}

class RoutingFun {

    Routing* r;

public:
    RoutingFun( Routing *_r ) : r(_r) { }

    void operator() ( const double x , double &dxdt , const double  t  )
    {
       dxdt = r->GetResIn()->GetInput(t) - r->SumQOut(x);
    }
};

// state_type = value_type = deriv_type = time_type = double
typedef runge_kutta_dopri5< double , double , double , double , vector_space_algebra , default_operations , never_resizer > stepper_type;

//[ integrate_observer
struct push_back_state_and_time
{
    std::vector< double >& m_states;
    std::vector< double >& m_times;

    push_back_state_and_time( std::vector< double > &states , std::vector< double > &times )
    : m_states( states ) , m_times( times ) { }

    void operator()( const double &x , double t )
    {
        m_states.push_back( x );
        m_times.push_back( t );
    }
};
//]

//----------------------------------------------------------------------------
Routing::Routing(QObject *parent) :
    QObject(parent), z_0(339.5), t_end(0.), rk_n(10), force_out(false)
{
    //m_res_out = new ReservoirOutput(this);
    WeirOutput*m_weir =  new WeirOutput(this);
    FixOutput* m_ispust = new FixOutput(this);
    m_weir->setName("Preljev");
    m_ispust->setName("Ispust");
    
    m_outputs.push_back(m_weir);
    m_outputs.push_back(m_ispust);
    
    
    m_res_in = new ResInput(this);
    m_res_in->setName("Rijeka");
    m_inputs.push_back(m_res_in);
    connect(m_inputs.at(m_inputs.size()-1), 
                    SIGNAL(dataLoaded(ResInput*)),this,SLOT(drawData(ResInput*)));
    qDebug()<<m_weir->ioType();
    qDebug()<<m_ispust->ioType();
    qDebug()<<m_res_in->ioType();
}

//----------------------------------------------------------------------------
void Routing::initGUIValues(void)
{
    QString msg;
    emit setValueZ0(msg.setNum(z_0));
    msg.clear();
    emit setValueRKN(msg.setNum(rk_n));
    msg.clear();
    emit setValueTEnd(msg.setNum(t_end));
    msg.clear();
    // ovo je samo zbog probe
    /*
    emit setValueZDonja(msg.setNum(m_ispust->zLower()));
    msg.clear();
    emit setValueZGornja(msg.setNum(m_ispust->zUpper()));
    msg.clear();
    emit setValueQIspust(msg.setNum(m_ispust->qOutput()));
    msg.clear();
    emit setValueZWeir(msg.setNum(m_weir->zweir()));
    msg.clear();
    emit setValueWeirWidth(msg.setNum(m_weir->weirWidth()));
    */
}

//----------------------------------------------------------------------------
bool Routing::Init(void)
{
    // malo ciscenja
    pts.clear();
    qpts.clear();
    qpreljev_pts.clear();
    qispust_pts.clear();
    zpts.clear();
    force_out=false;

    QString msg;
    if (vz.isEmpty())
    {
        qDebug()<<"Nije unesena krivulja V(z)";
        return false;
    }
    if (m_res_in->GetInput().isEmpty())
    {
        qDebug()<<"Nije unesena krivulja V(z)";
        return false;
    }

    //! \todo srediti ispitivanje...
/*    if (m_res_out->GetZDonja()<vz.x(0) )
    {
        qDebug()<<"Donja kota jezera postavljena na kotu nizu od \n"<<
                "najnize. Postavljam je na najnizu kotu...";
        m_res_out->SetZDonja(vz.x(0));
        msg.clear();
        emit setValueZDonja(msg.setNum(m_res_out->GetZDonja()));
    }

    if (m_res_out->GetZGornja()>vz.x(vz.numberOfPoints()-1) )
    {
        qDebug()<<"Gornja kota jezera postavljena na kotu visu od \n"<<
                "najvise. Postavljam je na najvisu kotu...";
        m_res_out->SetZGornja(vz.x(vz.numberOfPoints()-1));
        msg.clear();
        emit setValueZGornja(msg.setNum(m_res_out->GetZGornja()));
    }
*/
    if (rk_n<2)
    {
        qDebug()<<"Broj koraka manji od 2, stavljam default na 10!";
        rk_n=10;
        msg.clear();
        emit setValueRKN(msg.setNum(rk_n));
    }
    if (z_0<vz.x(0))
    {
        qDebug()<<"Pocetna kota jezera postavljena na kotu nizu od \n"<<
                "najnize. Postavljam je na najnizu kotu...";
        z_0 = vz.x(0);
        msg.clear();
        emit setValueZ0(msg.setNum(z_0));
    }
    else if (z_0>vz.x(vz.numberOfPoints()-1))
    {
        qDebug()<<"Pocetna kota jezera postavljena na kotu visu od \n"<<
                "najvise. Postavljam je na najvisu kotu...";
        z_0 = vz.x(vz.numberOfPoints()-1);
        msg.clear();
        emit setValueZ0(msg.setNum(z_0));
    }
    V_0=vz.evaluateY(z_0);
    return true;
}

//----------------------------------------------------------------------------
double Routing::Fun(const double &t, const double &v)
{

    return m_res_in->GetInput(t)-SumQOut(v);
    //return m_res_in.GetInput(t)-m_res_out.GetOutput(vz.InverseEvaluate(v));
}

//----------------------------------------------------------------------------
double Routing::SumQOut(const double &v)
{
    std::cout<<"SumQOut: v = "<<v<<std::endl;
    double z = vz.evaluateX(v);
    double q_out=0.;
    for(auto& output : m_outputs)
    {
        q_out += output->outputDischarge(z);
    }
    /*if (z>m_res_out->GetZGornja())
    {
        force_out=true;
    }
    if (z<m_res_out->GetZDonja())
    {
        force_out=false;
    }
    double q_out= m_res_out->GetOutput(z);
    if (force_out)
    {
        q_out+=m_res_out->GetQIspust();
    }*/
    return q_out;
}


//----------------------------------------------------------------------------
void Routing::FillQOut(const double &_v, double &/*_qispust*/, double & /*_qweir*/)
{
    std::cout<<"FillQout: _v = "<<_v<<std::endl;
    //double z = vz.evaluateX(_v);
    /*if (z>m_res_out->GetZGornja())
    {
        force_out=true;
    }
    if (z<m_res_out->GetZDonja())
    {
        force_out=false;
    }
    _qweir= m_res_out->GetOutput(z);
    if (force_out)
    {
        _qispust=m_res_out->GetQIspust();
    }
    else
    {
        _qispust=0.;
    }*/
}

//----------------------------------------------------------------------------
bool Routing::Solve(void)
{
  /*  int n=1;
    double h=0.;
    double k1,k2,k3,k4;
    const int NMAX=10000000;
    double x0,y0,eps;
    double x,y;
    // x0=0; u vremenu 0
    x0=0.;
    // y0 - Volumen u trenutku nula;
    y0=V_0;
    x=t_end;
    eps=0.05;
    n=1000;

    // izmedju svake tocke polilinije stavljam 10 intervala i ra�unam
    x0=0.; // x0=0; u vremenu 0
    y0=V_0; // y0 - Volumen u trenutku nula;
    x=x0;
    y=y0;
    Polyline& poly = m_res_in->GetInput();
    GenericPt pt;	// za V(t)
    GenericPt qpt;	// za Q_out(t)
    GenericPt qipt;	// za Q_ispust(t)
    GenericPt qwpt;	// za Q_weir(t)
    double qqi; // Q_ispust(t)
    double qqw; // Q_weir(t)
    int i,j;
    //n=10;
    n=rk_n;
    
    
    double state_x = y0;
    
    
    std::vector<double> x_vec;
    std::vector<double> times;
    RoutingFun f(this);

    */
    //integrate_adaptive( make_controlled( 1E-6 , 1E-6 , stepper_type() ) ,
    //                   f , state_x , x0 , poly.x(poly.numberOfPoints()-2) , 0.1 , write_cout );
    
    //runge_kutta4<double> stepper;
    //integrate_const( stepper, f , state_x , 0.0 , 10.0 , 0.01 );
    
    return true;
}
//----------------------------------------------------------------------------
void Routing::RoutingFunction(const double x , double &dxdt, const double t )
{
    //dxdt[0] = x[1];
    dxdt = m_res_in->GetInput(t)-SumQOut(x);
}
//----------------------------------------------------------------------------
void Routing::setQ_out(QVector<double>& x, QVector<double>& y)
{
    x.clear();
    y.clear();
    for (int j=0; j<qpts.count(); j++)
    {
        x.push_back(qpts.at(j).GetX());
        y.push_back(qpts.at(j).GetY());
    }
}

//----------------------------------------------------------------------------
void Routing::SetQi_t(QVector<double>& x, QVector<double>& y)
{
    x.clear();
    y.clear();
    for (int j=0; j<qispust_pts.count(); j++)
    {
        x.push_back(qispust_pts.at(j).GetX());
        y.push_back(qispust_pts.at(j).GetY());
    }
}

//----------------------------------------------------------------------------
void Routing::SetQw_t(QVector<double>& x, QVector<double>& y)
{
    x.clear();
    y.clear();
    for (int j=0; j<qpreljev_pts.count(); j++)
    {
        x.push_back(qpreljev_pts.at(j).GetX());
        y.push_back(qpreljev_pts.at(j).GetY());
    }
}

//----------------------------------------------------------------------------
void Routing::setZ_t(QVector<double>& x, QVector<double>& y)
{
    x.clear();
    y.clear();
    for (int j=0; j<pts.count(); j++)
    {
        x.push_back(pts.at(j).GetX());
        y.push_back(vz.evaluateX(pts.at(j).GetY()));
    }
}

//----------------------------------------------------------------------------
void Routing::FillV_z(QVector<double>& x, QVector<double>& y)
{
    x.clear();
    y.clear();
    for (size_t j=0; j<vz.numberOfPoints(); j++)
    {
        x.push_back(vz.x(j));
        y.push_back(vz.y(j));
    }
}


//----------------------------------------------------------------------------
void Routing::FillQin_t(QVector<double>& x, QVector<double>& y)
{
    for (size_t j=0; j<m_res_in->GetInput().numberOfPoints(); j++)
    {
        x.push_back(m_res_in->GetInput().x(j));
        y.push_back(m_res_in->GetInput().y(j));
    }
    t_end = m_res_in->GetInput().x(m_res_in->GetInput().numberOfPoints()-1);
    QString msg;
    msg.clear();
    emit setValueTEnd(msg.setNum(t_end));
}

//----------------------------------------------------------------------------
Polyline2d& Routing::GetQinPoly()
{
    return m_res_in->GetInput();
}

//----------------------------------------------------------------------------
void Routing::Clear()
{
    pts.clear();
    qpts.clear();
    zpts.clear();
    vz.clear();
    m_res_in->clear();
    //m_res_out->clear();
    qispust_pts.clear();
    qpreljev_pts.clear();
    zpts.clear();
    force_out=false;
    rk_n=10; // neka default vrijednost...
}

//----------------------------------------------------------------------------
void Routing::ClearResults()
{
    qpts.clear();
    qispust_pts.clear();
    qpreljev_pts.clear();
    zpts.clear();
}

//----------------------------------------------------------------------------
void Routing::setZ0(QString z0)
{
    z_0 = z0.toDouble();
    emit doClearResults();
}

//----------------------------------------------------------------------------
void Routing::setRKN(QString rkn)
{
    rk_n = rkn.toDouble();
    emit doClearResults();
}

//----------------------------------------------------------------------------
void Routing::setTEnd(QString tend)
{
    t_end = tend.toDouble();
    emit doClearResults();
}
/*
//----------------------------------------------------------------------------
void CRouting::setZDonja(QString zdonja)
{
    m_res_out->SetZDonja(zdonja.toDouble());
    emit doClearResults();
}

//----------------------------------------------------------------------------
void CRouting::setZGornja(QString zgornja)
{
    m_res_out->SetZGornja(zgornja.toDouble());
    emit doClearResults();
}

//----------------------------------------------------------------------------
void CRouting::setQispust(QString qis)
{
    m_res_out->SetQIspust(qis.toDouble());
    emit doClearResults();
}

//----------------------------------------------------------------------------
void CRouting::setZWeir(QString zweir)
{
    m_res_out->SetZWeir(zweir.toDouble());
    emit doClearResults();
}

//----------------------------------------------------------------------------
void CRouting::setWeirWidth(QString zwidth)
{
    m_res_out->SetWeirWidth(zwidth.toDouble());
    emit doClearResults();
}
*/
//----------------------------------------------------------------------------
void Routing::exportResults(QTextStream& str)
{
    str<< "t\t z(t) \t Ukupni q\t q_ispust\t q_preljev\n";
    for(int i=0; i< pts.count(); i++)
    {
        str<< pts.at(i).GetX() << "\t"<<
           vz.evaluateX(pts.at(i).GetY())<< "\t" <<
           qpts.at(i).GetY() << "\t" << qispust_pts.at(i).GetY()<< "\t"<<
           qpreljev_pts.at(i).GetY()<<endl;
    }
}

//----------------------------------------------------------------------------
void Routing::onPropertyValueChanged()
{
  emit signalPropertiesChanged();
}

//---------------------------------------------------------------------------
void Routing::drawData(ResInput* input)
{
    int input_index = m_inputs.indexOf(input);
    emit createInputGraph(input_index);
}

std::pair<QVector<double>, QVector<double>> Routing::fillInputGraPh(int input_idx)
{
    return m_inputs.at(input_idx)->GetInput().xyArrays();
}

