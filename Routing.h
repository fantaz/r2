#ifndef _ROUTING_H_
#define _ROUTING_H_


//#include "PolyLps.h"
#include "reservoiritem.h"
#include "reservoiroutput.h"
#include "reservoirinput.h"
#include <QList>
#include <QVector>
#include <QObject>

#include <QTextStream>

#include <QMultiMap>
//#include <boost/numeric/odeint.hpp>
//using namespace boost::numeric::odeint;

// klasa za tocke ...
class GenericPt
{
public:
    // konstruktor
    GenericPt(): x(0.), y(0.) {
        ;
    }
    // konstruktor
    GenericPt(double xx, double yy) : x(xx), y(yy) {
        ;
    }
    // Set/Get
    void SetPoint(const double &xx,const double &yy) {
        x=xx;
        y=yy;
    }
    void SetPoint(const GenericPt &pt) {
        x=pt.GetX();
        y=pt.GetY();
    }
    double GetX(void)const {
        return x;
    }
    double GetY(void)const {
        return y;
    }
private:
    double x,y; // mislim da je jasno
};

typedef QList<GenericPt> PtsArray ;




class Routing : public QObject
{
    Q_OBJECT
public:
    explicit Routing(QObject *parent = 0);
    // ovdje se inicijalizira proracun
    bool Init();
    // cisti nepotrebnu memoriju
    void Clear();
    // racunanje...
    bool Solve();
    // brise rezultate
    void ClearResults();

    // Set/Get
    void SetZ0(const double& z) {
        z_0=z;
    }
    double GetZ0(void) {
        return z_0;
    }
    void SetEndTime(const double& t) {
        t_end=t;
    }
    double GetEndTime(void) {
        return t_end;
    }
    void SetRKN(const long& n) {
        rk_n=n;
    }
    long GetRKN(void) {
        return rk_n;
    }


    // kreiranje i izracunavanje funkcije
    // za rjesavanje diferencijalne jedn.
    double Fun(const double &t, const double &v);
    void RoutingFunction(const double x , double &dxdt, const double t);
    // polyline V(z)
    Polyline2d vz;

    // punim Q_out(t) - za crtanje
    void setQ_out(QVector<double>& x, QVector<double>& y);

    // punim Q_ispust(t) - za crtanje
    void SetQi_t(QVector<double>& x, QVector<double>& y);

    // punim Q_preljev(t) - za crtanje
    void SetQw_t(QVector<double>& x, QVector<double>& y);

    // punim z_jez(t) - za crtanje
    void setZ_t(QVector<double>& x, QVector<double>& y);

    // punim Q_in(t)
    void FillQin_t(QVector<double>& x, QVector<double>& y);

    // punim V_(z)
    void FillV_z(QVector<double>& x, QVector<double>& y);


    // izracunaj ukupni Q_out
    double SumQOut(const double &v);
    // ukupni q_out
    void FillQOut(const double &_v, double &_qispust, double & _qweir);

    ResInput* GetResIn(void) {
        return m_res_in;
    }

    // ulazni hidrogram - Qin
    Polyline2d& GetQinPoly();

    //funckija iz koje se emitaju inicijalizirane vrijednosti u spinboxove
    void initGUIValues(void);

    // ispis rezultata u fajl
    void exportResults(QTextStream& str);

signals:
    void setValueZ0(QString);
    void setValueRKN(QString);
    void setValueTEnd(QString);
    // resout
    
    // ako se nesto promijeni u line-editima, onda se salje signal da se
    // izbrisu rezultati proracuna
    void doClearResults(void);
    void signalPropertiesChanged();
    
    //! \brief Salje index inputa mainwindow-u da se napokon kreira i prikaze 
    //! graf koji smo ucitali
    void createInputGraph(int input_idx);

public slots:
    //slotovi za primanje vrijednosti upisanuh u spinboxove
    void setZ0(QString);
    void setRKN(QString);
    void setTEnd(QString);
    // resout
    //! \brief vraca broj outputa
    size_t outputNumber() const 
    { return m_outputs.size(); }
    size_t inputNumber() const 
    {  return m_inputs.size(); }
     
    //! \brief vraca output na nekom mjestu
    ReservoirOutput* outputAt(size_t idx) 
    { return m_outputs.at(idx); }
    
    //! \brief vraca input na nekom mjestu
    ResInput* inputAt(size_t idx) 
    {   return m_inputs.at(idx); }
    
    //! \brief dodajemo novi output
    bool addNewOutput(const QString& type)
    {    
        if(type == "Fixed")
        {
            WeirOutput*m_weir =  new WeirOutput(this);
            m_weir->setName("Preljev");
            m_outputs.push_back(m_weir);
        }
        else if(type == "Weir")
        {
            FixOutput* m_ispust = new FixOutput(this);
            m_ispust->setName("Ispust");
            m_outputs.push_back(m_ispust);
        }
        else
        {return false;}
        
        return true;
    }
    bool addNewInput(const QString& type)
    {    
        if(type == "Input")
        {
            ResInput*m_input =  new ResInput(this);
            m_input->setName("Input");
            m_inputs.push_back(m_input);
            connect(m_inputs.at(m_inputs.size()-1), 
                    SIGNAL(dataLoaded(ResInput*)),this,SLOT(drawData(ResInput*)));
        }
        else
        {return false;}
        
        return true;
    }
    void onPropertyValueChanged();
    
    //! \brief povezan sa Input::dataLoaded signalom
    //! Samo zapisuje index inputa u listi inputa i to salje dalje sa signalom 
    //! createInputGraph kojeg bi trebao uhvatiti mainwindow
    void drawData(ResInput* input);
    //! \brief Napokon puni graf sa inputom koji je dan sa indexom u list inputa (\see input_idx)
    std::pair<QVector<double>, QVector<double>> fillInputGraPh(int input_idx);
    
protected:
    // pocetni z
    double z_0;
    // krajnje vrijeme
    double t_end;
    // broj intervala za RK izmedju dvije tocke ulaznog hidrograma
    long rk_n;

    //ReservoirOutput *m_res_out;
    //! \brief izlaz iz jezera
    QVector<ReservoirOutput*> m_outputs;

    // ulaz u jezero
    ResInput *m_res_in;
    //! \brief ulazi u jezero
    QVector<ResInput*> m_inputs;

    // Volumen(t)
    PtsArray pts;
    // Zbroj Q_out(t)
    PtsArray qpts;
    // Q_ispust(t)
    PtsArray qispust_pts;
    // Q_preljev(t)
    PtsArray qpreljev_pts;
    // Zbroj z_jez(t)
    PtsArray zpts;
    // ukljucen prisilni izlaz vode - npr. elektrana
    bool force_out;
private:
    // pocetni volumen (u t=0) - racuna se preko z_0
    double V_0;
};

#endif // _ROUTING_H_
