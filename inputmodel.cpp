#include "inputmodel.h"
#include <QDebug>

InputModel::InputModel(QObject *parent, Routing* r)
    :QAbstractTableModel(parent), routing(r)
{
}

int InputModel::rowCount(const QModelIndex & /*parent*/) const
{
    return routing->inputNumber();
}

int InputModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 3;
}

QVariant InputModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if(index.column()==0)
        {
            return routing->inputAt(index.row())->name();
        }
        else if(index.column()==1)
        {
            return routing->inputAt(index.row())->type();
        }
        else if(index.column()==2)
        {
            return routing->inputAt(index.row())->path();
        }
    }
    return QVariant();
}

bool InputModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole)
    {
        if(index.column()==0)
        {
            routing->inputAt(index.row())->setName(value.toString());
        }
        else if(index.column()==2)
        {
            routing->inputAt(index.row())->setFilePath(value.toString());
        }
    }
    return true;
}

QObject* InputModel::getItem(const QModelIndex &index, int role)
{
    if (role == Qt::DisplayRole)
    {
        return routing->inputAt(index.row());
    }
    return nullptr;
}

Qt::ItemFlags InputModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractTableModel::flags(index);

    if (index.isValid())
    {
        if (index.column() != 1)
        {return Qt::ItemIsEditable | Qt::ItemIsDropEnabled | defaultFlags;}
        else 
        {return Qt::ItemIsDropEnabled | defaultFlags;}
        
    }
    else
        return Qt::ItemIsDropEnabled/* | defaultFlags*/;
}

Qt::DropActions InputModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

QStringList InputModel::mimeTypes() const
{
    QStringList types;
    types << "application/x-qabstractitemmodeldatalist";
    //types << "application/vnd.text.list";
    return types;
}

QMimeData *InputModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    foreach (QModelIndex index, indexes) {
        if (index.isValid()) {
            QString text = data(index, Qt::DisplayRole).toString();
            stream << text;
        }
    }

    mimeData->setData("application/vnd.text.list", encodedData);
    //mimeData->setData("application/x-qabstractitemmodeldatalist", encodedData);
    return mimeData;
}

bool InputModel::dropMimeData(const QMimeData * data,
                               Qt::DropAction action,
                               int row, int column, const QModelIndex & parent)
{    
    if (!data->hasFormat("application/x-qabstractitemmodeldatalist"))
    {return false;}
    QByteArray itemData = data->data("application/x-qabstractitemmodeldatalist");
    QDataStream stream(&itemData, QIODevice::ReadOnly);

    int r, c;
    QMap<int, QVariant> v;
    stream >> r >> c >> v;
    if(v[0].toString() != "Input")
    { return false; }
    typeToAdd = v[0].toString();
    return insertRows( rowCount(), 1, QModelIndex() );

    //return true;
}

bool InputModel::insertRows(int position, int rows, const QModelIndex& /*parent*/ )
{
    beginInsertRows(QModelIndex(), position, position+rows-1);
    if(!routing->addNewInput(typeToAdd)){return false;}
    endInsertRows();
        return true;
}

void InputModel::on_PropertiesChanged()
{
    //emit layoutChanged();
}
