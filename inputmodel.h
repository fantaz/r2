#ifndef __INPUTMODEL_H
#define __INPUTMODEL_H

#include <QAbstractTableModel>
#include <QStringList>
#include <QMimeData>
#include <QDropEvent>
#include "Routing.h"
#include "filechooser.h"
class FileChooserDelegate :public QItemDelegate
{
public:
    explicit FileChooserDelegate(QObject *parent=0)
        : QItemDelegate(parent){
    }

    virtual QWidget* createEditor ( QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const
    {
        if ( index.column() == 2 ) {
            FileChooser* editor = new FileChooser( parent );
            editor->setMode(FileChooser::File);
            //editor->setFileName( m_countries );
            return editor;
        } else {
            QWidget *editor = QItemDelegate::createEditor(parent, option, index );
            editor->setAutoFillBackground(true);
            return editor;
        }
    }
    virtual void setEditorData( QWidget* editor, const QModelIndex& index ) const
    {
        if ( index.column() == 2 ) {
            FileChooser* combo = static_cast<FileChooser*>( editor );
            combo->setFileName(index.data( Qt::DisplayRole ).toString());
        } else {
            QItemDelegate::setEditorData( editor, index );
        }
    }
    virtual void setModelData( QWidget * editor, QAbstractItemModel* model, const QModelIndex& index ) const
    {
        if ( index.column() == 2 ) {
            FileChooser* combo = static_cast<FileChooser*>( editor );
            model->setData( index, combo->fileName() );
        } else {
            QItemDelegate::setModelData( editor, model, index );
        }
    }

    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem& option, const QModelIndex& index ) const
    {
        if ( index.column() == 2 ) {
            // don't allow the editor to get a smaller height than its sizehint.
            QRect rect(QPoint(0,0), option.rect.size().expandedTo(editor->sizeHint()));
            rect.moveCenter(option.rect.center());
            editor->setGeometry( rect );
        } else {
            QItemDelegate::updateEditorGeometry( editor, option, index );
        }
    }

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        //return QItemDelegate::sizeHint(option, index).expandedTo(QSize(64, option.fontMetrics.height()+16));
        return QItemDelegate::sizeHint(option, index);
    }
private:

};
class InputModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    InputModel(QObject *parent, Routing* r);
    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QObject* getItem(const QModelIndex &index, int role = Qt::DisplayRole);
    
    Qt::ItemFlags flags(const QModelIndex &index) const;
    Qt::DropActions supportedDropActions() const;
    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, 
                      int column, const QModelIndex & parent);
    bool insertRows(int position, int rows, const QModelIndex& /*parent*/ );

    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
public slots:
    void on_PropertiesChanged();
private:
    Routing* routing;
    QString typeToAdd;
};
#endif //__INPUTMODEL_H
