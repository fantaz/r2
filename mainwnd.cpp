#include "mainwnd.h"
#include "ui_mainwnd.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QMimeData>
#include "propertyeditor.h"

//#include <Python.h>

//----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //Py_Initialize();
    ui->setupUi(this);

    jez = new Routing(this);

    out_model = new OutputModel(this,jez);
    ui->outputView->setModel(out_model);
    ui->outputView->setAcceptDrops(true);
    ui->outputView->setDropIndicatorShown(true);
    
    in_model = new InputModel(this,jez);
    ui->inputView->setModel(in_model);
    ui->inputView->setAcceptDrops(true);
    ui->inputView->setDropIndicatorShown(true);
    ui->inputView->setItemDelegate( new FileChooserDelegate(this ) );

    // ui->outputView->selectionModel()->selectedRows();
    //selection changes shall trigger a slot
    QItemSelectionModel *selectionModel= ui->outputView->selectionModel();
    connect(selectionModel, SIGNAL(selectionChanged (const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectionChangedSlot(const QItemSelection &, const QItemSelection &)));
    connect(this, SIGNAL(displayOutput (QObject*)),
	ui->outputPropertiesWidget, SLOT(setObject(QObject*)) );
    

    ui->tabWidget->removeTab(0);
    ui->tabWidget->removeTab(0);
    vz_plot = new QCustomPlot(ui->tabWidget);
    //vz_plot->setMargin(5);
    ui->tabWidget->addTab(vz_plot,"V(z)");
    vz_plot->xAxis->setLabel("z [m n. m.]");
    vz_plot->yAxis->setLabel("Volumen [m^3]");
    //vz_plot->setAxisTitle(QwtPlot::xBottom, "z [m n. m.]");
    //vz_plot->setAxisTitle(QwtPlot::yLeft, "Volumen [m^3]");
    //SetPicker(vz_plot);


    qin_plot = new QCustomPlot(ui->tabWidget);
    //qin_plot->setMargin(5);
    ui->tabWidget->addTab(qin_plot,"Q_in");
    qin_plot->xAxis->setLabel("Vrijeme [s]");
    qin_plot->yAxis->setLabel("Protok [m^3/s]");
    //qin_plot->setAxisTitle(QwtPlot::xBottom, "Vrijeme [s]");
    //qin_plot->setAxisTitle(QwtPlot::yLeft, "Protok [m^3/s]");
    //SetPicker(qin_plot);

    zt_plot = new QCustomPlot(ui->tabWidget);
    //zt_plot->setMargin(5);
    ui->tabWidget->addTab(zt_plot,"Zjez(t)");
    qin_plot->xAxis->setLabel("Vrijeme [s]");
    qin_plot->yAxis->setLabel("z [m n. m.]");
    //zt_plot->setAxisTitle(QwtPlot::xBottom, "Vrijeme [s]");
    //zt_plot->setAxisTitle(QwtPlot::yLeft, "z [m n. m.]");
    //SetPicker(zt_plot);

    out_plot = new QCustomPlot(ui->tabWidget);
    //out_plot->setMargin(5);
    ui->tabWidget->addTab(out_plot,"Output(t)");
    out_plot->xAxis->setLabel("Vrijeme [s]");
    out_plot->yAxis->setLabel("Protok [m^3/s]");
    //out_plot->setAxisTitle(QwtPlot::xBottom, "Vrijeme [s]");
    //out_plot->setAxisTitle(QwtPlot::yLeft, "Protok [m^3/s]");
    //SetPicker(out_plot);
    
    makeConnections();
    jez->initGUIValues();
    ui->menubar->hide();
}

//----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete ui;
}

//----------------------------------------------------------------------------
void MainWindow::selectionChangedSlot(const QItemSelection & /*newSelection*/, const QItemSelection & /*oldSelection*/)
{
    //get the text of the selected item
    const QModelIndex index = ui->outputView->selectionModel()->currentIndex();
    
    QString selectedText = index.data(Qt::DisplayRole).toString();
    OutputModel* model = dynamic_cast<OutputModel*>(ui->outputView->model());
    if(model)
    {
	emit displayOutput(model->getItem(index));
    }
    //find out the hierarchy level of the selected item
    int hierarchyLevel=1;
    QModelIndex seekRoot = index;
    while(seekRoot.parent() != QModelIndex())
    {
        seekRoot = seekRoot.parent();
        hierarchyLevel++;
    }
    QString showString = QString("%1, Level %2").arg(selectedText)
                         .arg(hierarchyLevel);
    setWindowTitle(showString);
}

/*
//----------------------------------------------------------------------------
void MainWindow::SetPicker(QwtPlot* _plot)
{
	QwtPlotPicker* d_picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
		QwtPicker::PointSelection | QwtPicker::DragSelection,
		QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
		_plot->canvas());
	d_picker->setRubberBandPen(QColor(Qt::darkBlue));
	d_picker->setRubberBand(QwtPicker::CrossRubberBand);
	d_picker->setTrackerPen(QColor(Qt::darkBlue));
}
*/
//----------------------------------------------------------------------------
void MainWindow::makeConnections(void)
{
    //povezivanje odgovarajucih vrijednosti sa spinboxovima
    

    connect(jez,SIGNAL(doClearResults()),
            this, SLOT(on_clearResults_triggered()));
    
    connect(ui->outputPropertiesWidget,SIGNAL(itemChanged()),
        jez,SLOT(onPropertyValueChanged()));
    connect(jez,SIGNAL(signalPropertiesChanged()),
        out_model,SLOT(on_PropertiesChanged()));
    connect(jez, SIGNAL(createInputGraph(int)),this, SLOT(doCreateInputGraph(int)));
    
    
}

//----------------------------------------------------------------------------
void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

//----------------------------------------------------------------------------
QStringList MainWindow::process_line(const QString &line)
{
    QString str;
    QStringList list;
    list = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    return list;
}

//----------------------------------------------------------------------------
void MainWindow::FillVzPoly(QStringList line)
{
    if (line.size()==2)
    {
        bool xok,yok;
        xok=yok=false;
        double _x = line.at(0).toDouble(&xok);
        double _y = line.at(1).toDouble(&yok);
        if (xok && yok)
        {
            jez->vz.addPoint(_x,_y);
            //qDebug() << _x<<"\t" << _y;
        }
    }
}

//----------------------------------------------------------------------------
void MainWindow::FillQinPoly(QStringList line)
{
    if (line.size()==2)
    {
        bool xok,yok;
        xok=yok=false;
        double _x = line.at(0).toDouble(&xok);
        double _y = line.at(1).toDouble(&yok);
        if (xok && yok)
        {
            jez->GetQinPoly().addPoint(_x,_y);
            //qDebug() << _x<<"\t" << _y;
        }
    }
}

//----------------------------------------------------------------------------
void MainWindow::on_actionImport_v_z_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                       tr("Import V(z)"), "", tr("Text Files (*.txt *.* )"));

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }
    jez->vz.clear();
    PurgeCalc();

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        FillVzPoly(process_line(line));

    }
    vz_plot->addGraph();
    //SetLineStyle(d_crv1,QColor(Qt::blue),true);
    // punim liniju
    QVector<double> x,y;
    jez->FillV_z(x,y);
    vz_plot->graph(0)->setData(x,y); // dodajem je u plot
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    vz_plot->graph(0)->rescaleAxes(true);
    vz_plot->rescaleAxes();
    //vz_plot->setRangeDrag(Qt::Horizontal | Qt::Vertical);
    //vz_plot->setRangeZoom(Qt::Horizontal | Qt::Vertical);
    //vz_plot->setInteraction(QCustomPlot::iSelectPlottables); // allow selection of graphs via mouse click
    //SetPlotGrid(vz_plot);
    vz_plot->replot();
}

//----------------------------------------------------------------------------
void MainWindow::on_actionImport_Q_in_triggered()
{

    QString fileName = QFileDialog::getOpenFileName(this,
                       tr("Import Q in"), "", tr("Text Files (*.txt *.* )"));

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }
    jez->GetQinPoly().clear();
    PurgeCalc();

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        FillQinPoly(process_line(line));
    }
    qin_plot->addGraph();
    QVector<double> x,y;
    jez->FillQin_t(x,y);
    qin_plot->graph(0)->setData(x,y); // dodajem je u plot
    qin_plot->graph(0)->rescaleAxes(true);
    qin_plot->rescaleAxes();
    qin_plot->replot();
    qin_plot->graph();
}

//----------------------------------------------------------------------------
void MainWindow::on_actionPokreni_triggered()
{
    if (!PurgeCalc())
    {
        return;
    }

    if (!ValidateInput())
    {
        return;
    }

    // init  i solve - prava petlja
    if(!jez->Init())
    {
        qDebug()<<"Prekinuo izvrsavanje, nedostaju ulazni podaci";
        return;
    }
    jez->Solve();
    zt_plot->addGraph();
    QVector<double> x,y;
    jez->setZ_t(x,y);
    zt_plot->graph(0)->setData(x,y); // dodajem je u plot
    zt_plot->graph(0)->setPen(QPen(Qt::darkYellow, 2, Qt::DashLine));
    QColor zt_brush(Qt::darkYellow);
    zt_brush.setAlpha(20);
    zt_plot->graph(0)->setBrush(QBrush(zt_brush)); // first graph will be filled with translucent blue
    zt_plot->graph(0)->rescaleAxes(true);
    zt_plot->rescaleAxes();
    zt_plot->replot();

    out_plot->legend->setVisible(true);
    out_plot->legend->setFont(QFont("Helvetica",9));
    out_plot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    //out_plot->xAxis->setDateTimeFormat("MMMM\nyyyy");
    out_plot->xAxis->setDateTimeFormat("dd\nMM");

    out_plot->addGraph(); // dodajem qout_crv - "Qukupni(t)"
    x.clear();
    y.clear();
    jez->setQ_out(x,y);
    out_plot->graph(0)->setPen(QPen(Qt::darkBlue, 2, Qt::DashLine));
    out_plot->graph(0)->setName("Ukupni Q");
    out_plot->graph(0)->setData(x,y);
    out_plot->graph(0)->rescaleAxes(true);
    qDebug()<<x<<"\n";
    qDebug()<<y<<"\n";

    out_plot->addGraph(); //new QwtPlotCurve("Qispust(t)");
    x.clear();
    y.clear();
    jez->SetQi_t(x,y);
    out_plot->graph(1)->setName("Q ispust");
    out_plot->graph(1)->setData(x,y);
    out_plot->graph(1)->setPen(QPen(Qt::darkRed));
    QColor boja(Qt::darkRed);
    boja.setAlpha(20);
    out_plot->graph(1)->setBrush(QBrush(boja)); // first graph will be filled with translucent blue
    out_plot->graph(1)->rescaleAxes(true);

    out_plot->addGraph(); //"Qpreljev(t)"
    x.clear();
    y.clear();
    jez->SetQw_t(x,y);
    out_plot->graph(2)->setName("Q preljev");
    out_plot->graph(2)->setPen(QPen(Qt::darkGreen));
    out_plot->graph(2)->setData(x,y);
    out_plot->graph(2)->rescaleAxes(true);

    out_plot->rescaleAxes();
    out_plot->replot();
}

//----------------------------------------------------------------------------
void MainWindow::on_clearResults_triggered()
{
    PurgeCalc();
}

//----------------------------------------------------------------------------
bool MainWindow::PurgeCalc(void)
{
    //vz_plot->clearGraphs();
    out_plot->clearGraphs();
    zt_plot->clearGraphs();
    jez->ClearResults();
    out_plot->replot();
    zt_plot->replot();
    //vz_plot->replot();
    return true;
}

//----------------------------------------------------------------------------
bool MainWindow::ValidateInput(void)
{
    //! \todo provjera treba biti bolja...
    bool success = true;
    // malo provjera
    if (jez->GetEndTime() <=0.)
    {
        qDebug()<<"vrijeme nije broj ili vrijeme manje od nule";
        success = false;
    }
    if (jez->GetRKN()<2)
    {
        qDebug()<<"Krivo unesen broj koraka - nije broj";
        success = false;
    }
    /*    if (jez->GetResOut()->GetQIspust()<0.)
        {
            qDebug()<<"Q ispusta nije broj ili q manje od nule";
            success = false;
        }
        if (jez->GetResOut()->GetZDonja() > jez->GetResOut()->GetZGornja())
        {
            qDebug()<<"Gornja kota jezera niza od donje kote. ";
            success = false;
        }*/
    return success;
}
/*
//----------------------------------------------------------------------------
void MainWindow::showQOutCurve(QwtPlotItem *item, bool on)
{
	item->setVisible(on);
	QWidget *w = out_plot->legend()->find(item);
	if ( w && w->inherits("QwtLegendItem") )
		((QwtLegendItem *)w)->setChecked(on);

	out_plot->replot();
}
*/
//----------------------------------------------------------------------------
void MainWindow::on_actionExport_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("snimi rezultate"),
                       tr(""), tr("Text Files (*.txt *.* )"));

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        return;
    }
    QTextStream out(&file);

    jez->exportResults(out);
}

//----------------------------------------------------------------------------
void MainWindow::on_actionExit_triggered()
{
    this->close();
}

//----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent *event)
{
    event->accept();
}

//----------------------------------------------------------------------------
void MainWindow::on_evalVBtn_clicked()
{
    qDebug()<<"jupi";
    qDebug()<<jez->vz.evaluateY(330);
    qDebug()<<jez->vz.evaluateX(1000000);
}

//----------------------------------------------------------------------------
void MainWindow::doCreateInputGraph(int input_idx)
{
    QCPGraph* g = qin_plot->addGraph();  
    std::pair<QVector<double>, QVector<double>> xy = jez->fillInputGraPh(input_idx);
    QVector<double> x,y;
    //x= xy.first();
    //y= xy.second();
    g->setData(xy.first, xy.second); // dodajem je u plot
    g->rescaleAxes(true);
    qin_plot->rescaleAxes();
    qin_plot->replot();
    qin_plot->graph();
}
