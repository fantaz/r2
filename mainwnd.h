#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include "qcustomplot.h"
#include <math.h>

#include "Routing.h"
#include "outputmodel.h"
#include "inputmodel.h"
#include <QItemSelection>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *event);

    // ovo je proracun
    Routing *jez;
    // procesuiram linije iz fajlova - trebaju biti to�ke - x, y
    QStringList process_line(const QString &line);

    // puni se v(z) polilinija
    void FillVzPoly(QStringList line);

    // puni se Qin polilinija
    void FillQinPoly(QStringList line);

    // v(z) plot
    QCustomPlot* vz_plot;
    // Q(in) plot
    QCustomPlot* qin_plot;
    // z_jezera(t) plot
    QCustomPlot* zt_plot;
    // output(t) - elektrana + preljev
    QCustomPlot* out_plot;

    // kreiranje signala i slotova
    void makeConnections(void);

    // Ciscenje memorije: za npr. File|New
    bool PurgeCalc(void);
    // Ugrubo validiranje inputa
    bool ValidateInput(void);

    // postavljam tip linije
    //void SetLineStyle(QwtPlotCurve* _curve, const QColor& _color, bool _setsymbol=false);
    //void SetPlotGrid(QwtPlot* _plot);
    //void SetPicker(QwtPlot* _plot);

private slots:
    void on_actionImport_v_z_triggered();
    void on_actionImport_Q_in_triggered();
    void on_actionPokreni_triggered();
    void on_actionExport_triggered();
    void on_actionExit_triggered();
    // Q out ima vise krivulja, pa treba odabrati koju prikazati
    //void showQOutCurve(QwtPlotItem *, bool on);
    void on_clearResults_triggered();
    void on_evalVBtn_clicked();
    // kada se nesto promijeni u property editoru
    void selectionChangedSlot(const QItemSelection & /*newSelection*/, const QItemSelection & /*oldSelection*/);
    
    //! \brief kreira se input graf na temelju indexa u listi inputa 
    //! kojeg mi je poslao Routing s createInputGraph signalom
    void doCreateInputGraph(int input_idx);
signals:
    void displayOutput(QObject*);
    void InputGraphAdded(QCPGraph*);
private:
    Ui::MainWindow *ui;
    OutputModel* out_model;
    InputModel* in_model;
};

#endif // MAINWINDOW_H
