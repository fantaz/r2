#include "outputmodel.h"
#include <QDebug>

OutputModel::OutputModel(QObject *parent, Routing* r)
    :QAbstractTableModel(parent), routing(r)
{
}

int OutputModel::rowCount(const QModelIndex & /*parent*/) const
{
    return routing->outputNumber();
}

int OutputModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 2;
}

QVariant OutputModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if(index.column()==0)
        {
            return routing->outputAt(index.row())->name();
        }
        else if(index.column()==1)
        {
            return routing->outputAt(index.row())->type();
        }
    }
    return QVariant();
}

QObject* OutputModel::getItem(const QModelIndex &index, int role)
{
    if (role == Qt::DisplayRole)
    {
        return routing->outputAt(index.row());
    }
    return nullptr;
}

Qt::ItemFlags OutputModel::flags(const QModelIndex &index) const
{
    //return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
    Qt::ItemFlags defaultFlags = QAbstractTableModel::flags(index);

    if (index.isValid())
        return /*Qt::ItemIsEditable |*/ Qt::ItemIsDropEnabled | defaultFlags;
    else
        return Qt::ItemIsDropEnabled/* | defaultFlags*/;
}

Qt::DropActions OutputModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}

QStringList OutputModel::mimeTypes() const
{
    QStringList types;
    types << "application/x-qabstractitemmodeldatalist";
    //types << "application/vnd.text.list";
    return types;
}

QMimeData *OutputModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    foreach (QModelIndex index, indexes) {
        if (index.isValid()) {
            QString text = data(index, Qt::DisplayRole).toString();
            stream << text;
        }
    }

    mimeData->setData("application/vnd.text.list", encodedData);
    //mimeData->setData("application/x-qabstractitemmodeldatalist", encodedData);
    return mimeData;
}

bool OutputModel::dropMimeData(const QMimeData * data,
                               Qt::DropAction action,
                               int row, int column, const QModelIndex & parent)
{    
    if (!data->hasFormat("application/x-qabstractitemmodeldatalist"))
    {return false;}
    QByteArray itemData = data->data("application/x-qabstractitemmodeldatalist");
    QDataStream stream(&itemData, QIODevice::ReadOnly);

    int r, c;
    QMap<int, QVariant> v;
    stream >> r >> c >> v;
    if(v[0].toString() != "Weir" && v[0].toString() != "Fixed")
    { return false; }
    typeToAdd = v[0].toString();
    return insertRows( rowCount(), 1, QModelIndex() );

    //return true;
}

bool OutputModel::insertRows(int position, int rows, const QModelIndex& /*parent*/ )
{
    beginInsertRows(QModelIndex(), position, position+rows-1);
    if(!routing->addNewOutput(typeToAdd)){return false;}
    endInsertRows();
        return true;
}

void OutputModel::on_PropertiesChanged()
{
    emit layoutChanged();
}
