#ifndef __OUTPUTMODEL_H
#define __OUTPUTMODEL_H

#include <QAbstractTableModel>
#include <QStringList>
#include <QMimeData>
#include <QDropEvent>
#include "Routing.h"
class OutputModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    OutputModel(QObject *parent, Routing* r);
    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QObject* getItem(const QModelIndex &index, int role = Qt::DisplayRole);
    
    Qt::ItemFlags flags(const QModelIndex &index) const;
    Qt::DropActions supportedDropActions() const;
    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, 
                      int column, const QModelIndex & parent);
    bool insertRows(int position, int rows, const QModelIndex& /*parent*/ );
signals:
public slots:
    void on_PropertiesChanged();
private:
    Routing* routing;
    QString typeToAdd;
};
#endif //__OUTPUTMODEL_H
