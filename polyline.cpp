#include "polyline.h"
#include <cassert>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/algorithms/intersection.hpp> 

void Polyline2d::addPoint(const double x, const double y)
{
    append(line, pt2d(x,y));
}
double Polyline2d::evaluateY(double x)
{
    // bounding box
    boost::geometry::model::box<pt2d> box;
    boost::geometry::envelope(line, box);
    //std::cout << "envelope:" << boost::geometry::dsv(box) << std::endl;
    assert(x>=box.min_corner().x() && x<=box.max_corner().x());
    // okomita linija
    line2d ln;
    ln.push_back(make<pt2d>(x,box.max_corner().y()+1.));
    ln.push_back(make<pt2d>(x,box.min_corner().y()-1.));
    //std::cout << "okomita linija:" << boost::geometry::dsv(ln) << std::endl;
    
    // sjeciste
    line2d ints;
    boost::geometry::intersection(line, ln, ints);
    //std::cout << "sjeciste:" << boost::geometry::dsv(ints) << std::endl;
    
    return ints.at(0).y();
}

double Polyline2d::evaluateX(double y)
{
    // bounding box
    boost::geometry::model::box<pt2d> box;
    boost::geometry::envelope(line, box);
    //std::cout << "Evaluate X envelope:" << boost::geometry::dsv(box) << std::endl;
    //std::cout<<"y="<<y<<std::endl;
    assert(y>=box.min_corner().y() && y<=box.max_corner().y());
    // okomita linija
    line2d ln;
    ln.push_back(make<pt2d>(box.max_corner().x()+1.,y));
    ln.push_back(make<pt2d>(box.min_corner().x()-1.,y));
    //std::cout << "horz linija:" << boost::geometry::dsv(ln) << std::endl;
    
    // sjeciste
    line2d ints;
    boost::geometry::intersection(line, ln, ints);
    //std::cout << "sjeciste:" << boost::geometry::dsv(ints) << std::endl;
    
    return ints.at(0).x();
}

