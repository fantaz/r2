#ifndef __POLYLINE_H 
#define __POLYLINE_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

#include <boost/geometry/geometries/linestring.hpp>
#include <QVector>
#include <QDebug>
using namespace boost::geometry;
typedef model::d2::point_xy<double> pt2d;
typedef model::linestring<pt2d> line2d;

template <typename T>
struct x_between
{
    x_between(T a, T b)
        : fa(a), fb(b)
    {}

    template <typename P>
    bool operator()(P const& p) const
    {
        return boost::geometry::get<0>(p) >= fa
            && boost::geometry::get<0>(p) <= fb;
    }
private :
    T fa, fb;
};
// klasa za baratanje polilinijama
class Polyline2d
{
public:
    Polyline2d(){}
    //! \brief dodajemo tocku
    void addPoint(const double x, const double y);
    
    //! \brief vraca broj tocaka
    size_t numberOfPoints() const {return line.size();}
    
    //! \brief test ako je polilinija prazna
    bool isEmpty() const {return line.size()==0;}
    
    //! \brief brisemo liniju
    void clear() {line.clear();}
    
    //! \brief vraca x na poziciji idx
    //! \brief \precondition idx < line.size()
    const double &x(const size_t idx) const { return line.at(idx).x(); }
    
    //! \brief vraca y na poziciji idx
    //! \brief \precondition idx < line.size()
    const double &y(const size_t idx) const { return line.at(idx).y(); }
    
    //! \brief vraca x i y kao qVektore
    std::pair<QVector<double>, QVector<double>> xyArrays() const
    {
        QVector<double> x;
        QVector<double> y;
        for (const auto & pt: line)
        {
            x.push_back(pt.x());
            y.push_back(pt.y());
        }
        return std::make_pair(x,y);
    }
    //! \brief get linije
    line2d& getLine() {return line;}
    
    //! \brief evaluacija y za zadani x
    //! \precondition x mora biti min_x < x < max_x
    double evaluateY(double x);
    
    //! \brief evaluacija x za zadani y
    //! \precondition y mora biti min_y < y < max_y
    double evaluateX(double y);
private:
    line2d line;
};
#endif //__POLYLINE_H
