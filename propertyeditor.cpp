#include "propertyeditor.h"
#include <QMetaObject>
#include <QMetaProperty>
#include <QTreeWidgetItem>
#include <QDebug>
struct PropertyEditorData
{
    PropertyEditorData() : object(0) { }

    PropertyEditor* propEditor;
    QObject* object;
    bool initializing;

    void initFromObject();
    void createForList(QTreeWidgetItem* parent, const QVariant value);
    QVariant constructValueFrom(QTreeWidgetItem* item);
};

PropertyEditor::PropertyEditor(QWidget* parent)
:QTreeWidget(parent)
{
    d = new PropertyEditorData;
    d->propEditor = this;

    QStringList cols;
    cols << "Name" << "Value";

    setColumnCount(cols.count());
    setHeaderLabels(cols);

    connect(this, SIGNAL(itemChanged(QTreeWidgetItem*,int)),
            this, SLOT(on_itemChanged(QTreeWidgetItem*,int)));

    QFont f(font());
    f.setPointSizeF(f.pointSizeF() * 1.2f);
    setFont(f);

    setAlternatingRowColors(true);
}

PropertyEditor::~PropertyEditor()
{
    delete d;
}

QObject* PropertyEditor::object() const
{
    return d->object;
}

void PropertyEditor::setObject(QObject* object)
{
    if(d->object == object)
        return;

    clear();
    if(d->object)
    {   disconnect(d->object, 0, this, 0); }
    d->object = object;
    if(object)
    {  connect(object, SIGNAL(destroyed(QObject*)), this, SLOT(on_object_destroyed())); }

    d->initializing = true;
    d->initFromObject();
    d->initializing = false;
}

void PropertyEditor::on_itemChanged(QTreeWidgetItem* item, int column)
{
    if(!item || !d->object || column!=1 || d->initializing)
    {   return; }

    QTreeWidgetItem* item2 = item;
    while(item2->parent())
    {   item2 = item2->parent(); }

    QString name = item2->text(0);
    QVariant value = d->constructValueFrom(item2);
    //d->object->setProperty(name.toAscii().data(), value);
    d->object->setProperty(name.toStdString().c_str(), value);
    emit itemChanged();
}

void PropertyEditor::on_object_destroyed()
{
    setObject(nullptr);
}

void PropertyEditorData::initFromObject()
{
    if(!object)
    {return;}

    const QMetaObject* mo = object->metaObject();
    int propCount = mo->propertyCount();
    
    for(int i=1; i<propCount; i++)
    {
        QMetaProperty prop = mo->property(i);
        if(!prop.isWritable() || prop.isEnumType())
	{    continue; }

        QString propName(prop.name());
        QVariant propValue = prop.read(object);
        QTreeWidgetItem* item = new QTreeWidgetItem(propEditor);
        item->setData(0, Qt::DisplayRole, propName);
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEditable|Qt::ItemIsEnabled);

        if(prop.type() == QVariant::List)
	{    createForList(item, propValue); }
        else
	{ item->setData(1, Qt::DisplayRole, propValue); }
    }
}

void PropertyEditorData::createForList(QTreeWidgetItem* parent, const QVariant value)
{
    QString pName = parent->text(0);
    QList<QVariant> list = value.toList();
    for(int i=0; i<list.count(); i++)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem(parent);
        item->setData(0, Qt::DisplayRole, QString("%1[%2]").arg(pName).arg(i));
        item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEditable|Qt::ItemIsEnabled);

        QVariant val = list[i];
        if(val.type() == QVariant::List)
	{createForList(item, val);}
        else
	{    item->setData(1, Qt::DisplayRole, val); }
    }

    parent->setExpanded(true);
}

QVariant PropertyEditorData::constructValueFrom(QTreeWidgetItem* item)
{
    if(item->childCount())
    {
        QList<QVariant> ret;
        for(int i=0; i<item->childCount(); i++)
	{   ret << constructValueFrom(item->child(i)); }
        return ret;
    }

    return item->data(1, Qt::DisplayRole);
}

