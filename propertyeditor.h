#ifndef __PROPERTY_EDITOR_H
#define __PROPERTY_EDITOR_H

#include <QTreeWidget>

struct PropertyEditorData;
class PropertyEditor : public QTreeWidget
{
    Q_OBJECT

public:
    PropertyEditor(QWidget* parent);
    ~PropertyEditor();

    QObject* object() const;

public slots:
    void setObject(QObject* object);
signals: 
    void itemChanged();
private slots:
    void on_itemChanged(QTreeWidgetItem* item, int column);
    void on_object_destroyed();

protected:
    void drawBranches(QPainter* , const QRect &, const QModelIndex &) const { }

protected:
    PropertyEditorData* d;
};

#endif
