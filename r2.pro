
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

unix {
QMAKE_CXXFLAGS += -std=c++0x
 }
TARGET = r2
TEMPLATE = app

# win32 specific
win32 {
     INCLUDEPATH += "$$(BOOST_ROOT)"
     LIBS += D:\Programiranje\_libs\boost_1_54_0_vs2012\libs
    CONFIG +=static
 }

SOURCES += main.cpp\
        mainwnd.cpp\
        polyline.cpp\
        Routing.cpp\
        qcustomplot.cpp\
        outputmodel.cpp\
        inputmodel.cpp\
        filechooser.cpp\
        propertyeditor.cpp


HEADERS  += polyline.h\
              Routing.h\
              mainwnd.h\
              qcustomplot.h\
              outputmodel.h\
              inputmodel.h\
              filechooser.h\
              propertyeditor.h\

FORMS    += mainwnd.ui

RESOURCES     = slike.qrc
