#include "reservoirinput.h"
#include <QFile>
#include <QDebug>
#include <QStringList>
#include <QTextStream>
//----------------------------------------------------------------------------
ResInput::ResInput(QObject *parent) : ReservoirItem(ReservoirItem::Input,parent)
{
    connect(this, SIGNAL(filePathChanged()),this,SLOT(readInput()));
}

//----------------------------------------------------------------------------
double ResInput::GetInput(const double &t)
{
    //wxUnusedVar(t);
    double broj = Qin.evaluateY(t);
    std::cout<<"CResIn::GetInput: ("<<t<<","<<broj<<")"<<std::endl;
    return Qin.evaluateY(t);
}

//----------------------------------------------------------------------------
void ResInput::readInput()
{
    Qin.clear();
    //qDebug()<<filePath;
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }
    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QStringList list;
        list = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
        if (list.size()==2)
        {
            bool xok,yok;
            xok=yok=false;
            double _x = list.at(0).toDouble(&xok);
            double _y = list.at(1).toDouble(&yok);
            if (xok && yok)
            {
                Qin.addPoint(_x,_y);
                //qDebug() << _x<<"\t" << _y;
            }
        }
    }
    emit dataLoaded(this);
} 
