#ifndef __RESERVOIRINPUT_H_
#define __RESERVOIRINPUT_H_
#include "reservoiritem.h" 
#include "polyline.h"
// ulaz u jezero
class ResInput : public ReservoirItem
{
    Q_OBJECT
public:
    explicit ResInput(QObject *parent = 0);
    // vraca se Q(t_zadano)
    double GetInput(const double &t);
    // vraca pt na poliliniju
    Polyline2d& GetInput(void) { return Qin;  }
    // brisanje polilinije
    void clear(void) { Qin.clear(); }
    
    QString type() const override { return "Polyline Input"; }
    QString path() const {return filePath;}
    void setFilePath(const QString& pth){filePath = pth;emit filePathChanged();}
signals:
    //! \brief salje signal svaki put kada se promijeni path datoteke
    void filePathChanged();
    
    //! \brief salje se svaki puta kada smo sigurno ucitali poliliniju
    //! to se desava u readInput
    //! Smisamo je da povezemo Routing objekt sa OVIM inputom
    void dataLoaded(ResInput*);
private slots:
    //! \brief cita ulaznu datoteku iz \see filePath i sprema u \see Qin
    void readInput();
private:
    
    // polyline Q_in(t)
    Polyline2d Qin;
    // file gdje je spremljena polilinija
    QString filePath;
};

#endif //__RESERVOIRINPUT_H_
