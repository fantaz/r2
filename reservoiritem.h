#ifndef __RESERVOIRITEM_H_
#define __RESERVOIRITEM_H_
#include <QObject>
#include <QMetaEnum>
class ReservoirItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY( QString Name READ name WRITE setName)
    Q_ENUMS(IOType)
public:
    enum  IOType { Output, Input };
    explicit ReservoirItem(IOType t, QObject *parent = 0);
    virtual QString type() const=0;
    QString ioTypeString() const
    {
        int index = metaObject()->indexOfEnumerator("IOType");
        QMetaEnum metaEnum = metaObject()->enumerator(index);
        return metaEnum.valueToKey(m_ioType);
    }
    IOType ioType() const
    {
        return m_ioType;
    }
    
public slots:
    QString name() const { return m_name; }
    void setName(const QString Name) { m_name = Name; }

private:
    QString m_name;
    const IOType m_ioType;
    
};

#endif //__RESERVOIRITEM_H_
