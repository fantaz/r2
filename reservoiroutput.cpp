#include "reservoiroutput.h"
//----------------------------------------------------------------------------
ReservoirOutput::ReservoirOutput(IOType t,QObject *parent) :
    ReservoirItem(t,parent)
{
}


//----------------------------------------------------------------------------
WeirOutput::WeirOutput(QObject* parent): ReservoirOutput(ReservoirItem::Output,parent), 
            m_zweir(339.), m_weirWidth(20.)
{

}

//----------------------------------------------------------------------------
double WeirOutput::outputDischarge(const double& z)
{
    //! \todo ovdje je modeliran samo jedan preljev!!!!
    if (z<=m_zweir)
    { return 0.; }
    return 0.42*sqrt(2*9.81)*m_weirWidth*pow(z-m_zweir,1.5);
}

//----------------------------------------------------------------------------
FixOutput::FixOutput(QObject* parent): ReservoirOutput(ReservoirItem::Output,parent), 
                    m_zLower(315.), m_zUpper(335.), m_qOutput(20.), forceOutput(false)
{

}

//----------------------------------------------------------------------------
double FixOutput::outputDischarge(const double& z)
{
    if (z>m_zUpper)
    { forceOutput=true; }
    if (z<m_zLower)
    { forceOutput=false; }
    
    if (forceOutput)
    { return m_qOutput; }
    return 0.;
}