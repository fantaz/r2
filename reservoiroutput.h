#ifndef __RESERVOIROUTPUT_H_
#define __RESERVOIROUTPUT_H_
#include "reservoiritem.h"
// izlaz iz jezera
class ReservoirOutput : public ReservoirItem
{
    Q_OBJECT

public:
    explicit ReservoirOutput(IOType t,QObject *parent = 0);
    // vraca Q(z_zadano)
    double GetOutput(const double &z);
    // brisanje necega - za sada nepotrebno...
    void clear(void) { }

    virtual double outputDischarge(const double &z)=0;

signals:
    //! \brief mora javiti da ce brisati rezultate
    void doClearResults();


};

//! \brief klasa koja modelira preljev
class WeirOutput : public ReservoirOutput
{
    Q_OBJECT
    Q_PROPERTY( double ZWeir READ zweir WRITE setZWeir )
    Q_PROPERTY( double WeirWidth READ weirWidth WRITE setWeirWidth)
public:
    explicit WeirOutput(QObject* parent=0);

    //! \brief vraca visinu preljeva
    double zweir() const { return m_zweir; }
    //! \brief vraca sirinu preljeva
    double weirWidth() const { return m_weirWidth; }
    //! \brief vracam protok kroz preljev
    double outputDischarge(const double &z);
    
    QString type() const override { return "Weir"; }
    
public slots:
    void setZWeir(const double zw) { m_zweir = zw; }
    void setWeirWidth(const double _b) { m_weirWidth = _b; }
private:
    // Kota na kojoj se ukljucuju preljevi
    double m_zweir;
    // sirina preljeva
    double m_weirWidth;
};

//! \brief Modelira se fiksni izlaz iz jezera - npr. elektrana ili temeljni ispust
//! \todo ovo treba prosiriti da bude f(z)
class FixOutput : public ReservoirOutput
{
    Q_OBJECT
    Q_PROPERTY( double ZLower READ zLower WRITE setZLower )
    Q_PROPERTY( double Zupper READ zUpper WRITE setZUpper )
    Q_PROPERTY( double QOut READ qOutput WRITE setQOutput )
public:
    explicit FixOutput(QObject* parent=0);
    double zLower() const { return m_zLower; }
    double zUpper() const { return m_zUpper; }
    double qOutput() const { return m_qOutput; }
    //! \brief vracam protok kroz preljev
    double outputDischarge(const double &z);
    QString type() const override { return "Fixed Output"; }
public slots:
    void setZLower(const double z) { m_zLower = z; }
    void setZUpper(const double z) { m_zUpper = z; }
    void setQOutput(const double q) { m_qOutput = q; }
private:
    //! \brief nadmorska visina kada se prestaje sa radom centrale
    //! t.j. kada je aktivan q_ispust
    double m_zLower;
    //! \brief nadmorska visina kada se pocinje sa radom centrale
    //!  t.j. kada je aktivan q_ispust
    double m_zUpper;
    //! \brief fiksni protok koji se aktivira kada je z jezera
    //! izmedju z_donja i z_gornja
    double m_qOutput;
    bool forceOutput;
};

#endif //__RESERVOIROUTPUT_H_
